import { Injectable } from '@angular/core';
import { AES, enc } from 'crypto-js';

const SECRET_KEY = 'test-key';


@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  encryptData(value: any): any {
    return AES.encrypt(JSON.stringify(value), SECRET_KEY).toString();
  }

  decryptData(value: any): any {
    try {
      if (value !== null && value !== '') {
        const bytes: any = AES.decrypt(value.toString(), SECRET_KEY);
        return JSON.parse(bytes.toString(enc.Utf8));
      }
      else {
        return value;
      }
    } catch (error) {
      console.log(error);
      localStorage.clear();
    }
  }

  getItem(key: string): any {
    const data = localStorage.getItem(key);
    return this.decryptData(data);
  }

  setItem(key: string, value: any): void {
    localStorage.setItem(key, this.encryptData(value));
  }

  removeItem(key: string): void {
    localStorage.removeItem(key);
  }

  removeAll(): void {
    localStorage.clear();
  }
}
