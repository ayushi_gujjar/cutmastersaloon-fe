import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { logEnum } from '../constants/constant';
import { LogLevel } from '../modals/log';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor() { }

  debug(...message: any): void {
    this.writeToLog(LogLevel.DEBUG, ...message);
  }

  log(...message: any): void {
    this.writeToLog(LogLevel.INFO, ...message);
  }

  warn(...message: any): void {
    this.writeToLog(LogLevel.WARN, ...message);
  }

  error(...message: any): void {
    this.writeToLog(LogLevel.ERROR, ...message);
  }

  shouldLog(level: LogLevel): boolean {
    return (level >= logEnum[environment.logLevel]);
  }
  writeToLog(level: LogLevel, ...message: any): void {
    if (this.shouldLog(level)) {
      if (level <= LogLevel.INFO) {
        console.log(...message);
      } else if (level === LogLevel.ERROR) {
        console.error(...message);
      } else if (level === LogLevel.WARN) {
        console.warn(...message);
      }
    }
  }
}
