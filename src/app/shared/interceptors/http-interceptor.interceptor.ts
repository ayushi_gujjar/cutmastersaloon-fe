import { Injectable } from "@angular/core";
import {
    HttpRequest,
    HttpHandler,
    HttpInterceptor,
    HttpHeaders,
    HttpErrorResponse,
    HttpEvent
} from '@angular/common/http';
import { Observable } from "rxjs";
@Injectable()
export class HttpInterceptorInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const url: string = '';
        const headersObj: any = '';
        return next.handle(req.clone({
            url,
            headers: new HttpHeaders(headersObj)
        }))
    }
}
